import java.util.Scanner;

/**
 * Created by themba on 10/8/2015.
 */
public class RomanNumerals {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int sum;
        System.out.print("Enter roman number1: ");
        String roman1 = scan.nextLine();
        System.out.print("Enter roman number2: ");
        String roman2 = scan.nextLine();
        sum = calculateRoman(roman1,roman2);

        System.out.println("The sum is: " + sum);

    }

    public static int calculateRoman(String roman1, String roman2){
        int sum;
        int number1 = romanConvert(roman1);
        int number2 = romanConvert(roman2);

        sum = number1 + number2;

        System.out.println("Number1: " + number1);
        System.out.println("Number2: " + number2);
        return  sum;
    }
    public static int romanConvert(java.lang.String roman) {
        int decimal = 0;
        int lastNumber = 0;
        String romanNumeral = roman.toUpperCase();
        for (int x = romanNumeral.length() - 1; x >= 0 ; x--) {
            char convertToDecimal = romanNumeral.charAt(x);

            switch (convertToDecimal) {
                case 'M':
                    decimal = processDecimal(1000, lastNumber, decimal);
                    lastNumber = 1000;
                    break;

                case 'D':
                    decimal = processDecimal(500, lastNumber, decimal);
                    lastNumber = 500;
                    break;

                case 'C':
                    decimal = processDecimal(100, lastNumber, decimal);
                    lastNumber = 100;
                    break;

                case 'L':
                    decimal = processDecimal(50, lastNumber, decimal);
                    lastNumber = 50;
                    break;

                case 'X':
                    decimal = processDecimal(10, lastNumber, decimal);
                    lastNumber = 10;
                    break;

                case 'V':
                    decimal = processDecimal(5, lastNumber, decimal);
                    lastNumber = 5;
                    break;

                case 'I':
                    decimal = processDecimal(1, lastNumber, decimal);
                    lastNumber = 1;
                    break;
            }
        }
        return decimal;
    }

    public static int processDecimal(int decimal, int lastNumber, int lastDecimal) {
        if (lastNumber > decimal) {
            return lastDecimal - decimal;
        } else {
            return lastDecimal + decimal;
        }
    }
}
