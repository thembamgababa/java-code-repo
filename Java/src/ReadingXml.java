import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by themba on 9/22/2015.
 */
public class ReadingXml {
  public static void main(String[]args){
      try {
                  File inputFile = new File("C:\\Users\\themba\\Desktop\\Java\\java-code-repo\\Java\\src\\Add.Xml");

                  DocumentBuilderFactory create = DocumentBuilderFactory.newInstance();
                  DocumentBuilder dCreate = create.newDocumentBuilder();
                  Document doc = dCreate.parse(inputFile);
                  doc.getDocumentElement().normalize();

                  NodeList nList = doc.getElementsByTagName("Student");

                  for (int temp = 0; temp < nList.getLength(); temp++){
                      Node nNode = nList.item(temp);

                      if (nNode.getNodeType() == Node.ELEMENT_NODE){
                          Element eElement = (Element) nNode;

                          System.out.println( eElement.getAttribute("Name"));
                      }
                  }
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      }



