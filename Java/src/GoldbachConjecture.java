import java.util.*;

/**
 * Created by themba on 10/5/2015.
 */
public class GoldbachConjecture {
    public static void main(String[] args){
        boolean done = false ;
        try {
            while(!done) {
                int number = userInput();
                if (number % 2 == 0) {
                    display(primeNumber(number), number);
                    done = true;
                } else {
                    System.out.println("Enter even number >2.");
                    done = false;
                }
            }
        }
        catch(ArithmeticException e) {
            System.out.println("Exception caught: / by zero.");
        }
    }

    public static boolean[] primeNumber(int max){
        boolean[] prime = new boolean[max];
        for(int p =2; p < max ;p++ ){
            prime[p] = true;
        }
        for(int x = 2; x< max; x++){
            if(prime[x]){
                for(int y = x+x; y< max; y= y+x){
                    prime[y] = false;
                }
            }
        }
        return prime;
    }
    public static void display (boolean[] allPrimeNumbers,int evenNumber){
        int firstNumber = 0, secondNumber = 0;
        allPrimeNumbers = primeNumber(evenNumber);
        for (int p = allPrimeNumbers.length - 1; p >= evenNumber/2; p--) {
            if (allPrimeNumbers[p]) {
                firstNumber = p;
                secondNumber = evenNumber - firstNumber;
                if (allPrimeNumbers[secondNumber]) {
                    System.out.println(evenNumber + " = " + firstNumber + " + " + secondNumber);
                }
            }
        }
    }

    public static int userInput(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter even number >2.");
        int number = Integer.parseInt(input.nextLine());
        return number;
    }
}