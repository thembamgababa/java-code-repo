import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by themba on 10/1/2015.
 */
public class ReverseIt {
    public static void main(String[] args) {

        String string = "themba";
        String reverse = new StringBuffer(string).
                reverse().toString();
        for (int j = string.length() - 1; j >= 0; j--) {
            reverse = reverse + string.charAt(j);
            System.out.println("\nString before reverse: " + string);
            System.out.println("String after reverse: " + reverse);

            List<Integer> list = Arrays.asList(1, 4, 9, 16, 9, 7, 4, 9, 11);
            System.out.println(list);
            Collections.reverse(list);
            System.out.println(list);
            for (int i = 0; i <i; i++) {
                System.out.println(list.get(i));
            }
        }
    }
}
