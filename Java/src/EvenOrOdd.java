import java.util.Scanner;

/**
 * Created by themba on 10/1/2015.
 */
public class EvenOrOdd {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        System.out.printf("Enter your number to check even or odd:");
        int number = console.nextInt();
        if ((number % 2) == 0) {
            System.out.printf("number %d is even number %n", number);
        } else {
            System.out.printf("number %d is odd number %n", number);
        }
        System.out.printf("Finding number if its even or odd using bitwise %n");
        if ((number & 1) == 0) {
            System.out.printf("number %d is even number %n", number);
        } else {
            System.out.printf("number %d is odd number %n", number);
        }
    }
}
