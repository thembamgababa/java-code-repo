import java.util.Scanner;

/**
 * Created by themba on 10/6/2015.
 */
public class MorseCode {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter a String for translation:");
        String input = scan.nextLine();
        System.out.println( convertToMorse( input));
    }

    public static String convertToMorse(String s) {
        String answer = "";
        s = s.toLowerCase();
        for (int i=0; i<s.length(); i++) {
            char ch = s.charAt(i);
            if (ch=='a') answer += ".-";
            else if (ch=='b') answer += "-...";
            else if (ch=='c') answer += "-.-.";
            else if (ch=='d') answer += "-..";
            else if (ch=='e') answer += ".";
            else if (ch=='f') answer += "..-.";
            else if (ch=='g') answer += "--.";
            else if (ch=='h') answer += "....";
            else if (ch=='i') answer += "..";
            else if (ch=='j') answer += ".---";
            else if (ch=='k') answer += "-.-";
            else if (ch=='l') answer += ".-..";
            else if (ch=='m') answer += "--";
            else if (ch=='n') answer += "-.";
            else if (ch=='o') answer += "---";
            else if (ch=='p') answer += ".--.";
            else if (ch=='q') answer += "--.-";
            else if (ch=='r') answer += ".-.";
            else if (ch=='s') answer += "...";
            else if (ch=='t') answer += "-";
            else if (ch=='u') answer += "..-";
            else if (ch=='v') answer += "...-";
            else if (ch=='w') answer += ".--";
            else if (ch=='x') answer += "-..-";
            else if (ch=='y') answer += "-.--";
            else if (ch=='z') answer += "--..";
            else if (ch=='1') answer += " .----";
            else if (ch=='2') answer += " ..---";
            else if (ch=='3') answer += "...--";
            else if (ch=='4') answer += "....-";
            else if (ch=='5') answer += ".....";
            else if (ch=='6') answer += "-.....";
            else if (ch=='7') answer += " --...";
            else if (ch=='8') answer += " ---..";
            else if (ch=='9') answer += "---- .";
            else if (ch=='0') answer += "-----";
            else if (ch==' ') answer += " ";
            else System.out.println("Your input is invalid");
            answer += " ";
        }
        return answer;
    }
}

