import java.io.*;
import java.util.Scanner;

/**
 * Created by themba on 10/9/2015.
 */
public class UpdateConfigurationFile {
    public static void main(String[] args){
        File myFile = new File("C:\\Users\\themba\\Documents\\java-code-repo\\Java\\src\\ConfigurationFile.txt");
        try {
            Scanner scan = new Scanner(System.in);
            System.out.print("Select the option:\n" + "1. To disable NEEDSPELLING option\n" + "2. To enable SEEDSREMOVED option\n");

            int option = scan.nextInt();

            if(option == 1) {
                System.out.print("Select the word you want to disable: ");
                String choice = scan.next();

                String[] info = disable(myFile,choice);
                writeToFile(myFile,info);

            }
            else if (option == 2) {
                System.out.print("Select the word you want to enable:");
                String choice = scan.next();
                String[] info = enable(myFile, choice);
                writeToFile(myFile,info);
            }


        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    public static int count(File file) throws Exception {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = null;


        int count = 0;
        while((line = br.readLine()) != null)
        {
            count++;
        }
        return count;
    }

    public static String[] readInfo(File file) throws Exception
    {
        boolean status = true;
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        int count =0;
        String[] info = new String[count(file)];

        while((line = br.readLine()) != null) {
            info[count] = line;
            count++;

        }
        return info;
    }
    public static  String[] disable(File file,String disWord)throws Exception {
        String[] info = readInfo(file);
        for(int x= 0;x < info.length;x++)
        {
            if(!info[x].startsWith("#") && !info[x].isEmpty() && info[x].equalsIgnoreCase(disWord)) {
                info[x] = "; " + info[x];
            }
        }


        return info;
    }
    public static  String[] enable(File file,String disWord)throws Exception {
        String[] info = readInfo(file);
        for(int x= 0;x < info.length;x++) {
            if(!info[x].startsWith("#") && !info[x].isEmpty()) {
                String[] el = info[x].split(" ");

                String newLine = info[x];
                for(int y = 0;y < el.length;y++) {
                    if(el[y].equalsIgnoreCase(disWord)) {
                        newLine = disWord.toUpperCase();
                    }
                    info[x] = newLine;
                }
            }
        }


        return info;
    }

    public static void writeToFile(File myfile,String[] info)throws Exception {
        myfile.createNewFile();

        FileWriter fw = new FileWriter(myfile);
        BufferedWriter bw = new BufferedWriter(fw);

        for(String line:info) {
            bw.write(line);

            bw.newLine();
        }

        bw.close();

    }

}
