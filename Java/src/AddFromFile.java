import java.io.*;

/**
 * Created by themba on 10/2/2015.
 */
public class AddFromFile {
    public static void main(String[] args) {
        int sum = 0;
            try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\themba\\Documents\\java-code-repo\\src\\AddFile.txt")))

            {

                String line;

                while ((line = br.readLine()) != null) {
                    System.out.print(line + " + ");
                    sum += Integer.parseInt(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        System.out.println( "=" + sum);
        }
    }