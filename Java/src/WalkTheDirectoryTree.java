import java.io.File;
import java.io.FileFilter;

/**
 * Created by themba on 10/9/2015.
 */
public class WalkTheDirectoryTree {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\themba\\Documents\\java-code-repo\\Java");
        File[] fileList = file.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".txt");
            }
        });

        if(fileList != null) {
            for(File f: fileList) {
                System.out.println("File in directory: " + f.getName());
            }
        }

    }
}
