import java.util.Scanner;

/**
 * Created by themba on 10/8/2015.
 */
public class CommasAnds {
    public static void main(String[]args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(function(new String[]{}));
        System.out.println(function(new String[]{"ABC"}));
        System.out.println(function(new String[]{"ABC", "DEF"}));
        System.out.println(function(new String[]{"ABC", "DEF", "G", "H"}));
    }
    public static String function(String[] words) {
        String commas = "{";
        for (int windex = 0; windex < words.length; windex++) {
            commas += words[windex] + (windex == words.length - 1 ? "" : windex == words.length - 1 ? " and " : ", ");
        }
        commas += "}";
        return commas;

    }
}
