import java.io.File;

/**
 * Created by themba on 10/1/2015.
 */
public class DeleteFile {
    public static boolean deleteFile(String filename){
        boolean exists = new File(filename).delete();
        return exists;
    }
    public static void docs(String type, String filename) {
        System.out.println("The following " + type + " called " + filename +
                        (deleteFile(filename) ? " was deleted." : " could not be deleted.")
        );
    }
    public static void main(String args[]) {
        docs("file", "input.txt");
        docs("file", File.separator + "input.txt");
        docs("directory", "docs");
        docs("directory", File.separator + "docs" + File.separator);
    }
}
