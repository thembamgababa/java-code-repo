/**
 * Created by themba on 9/21/2015.
 */

import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemTime {
    public static void main(String[] args) {
        Date date = new Date();
        String Time = new SimpleDateFormat("HH mm ss").format(date);
        System.out.print(Time);

    }
}