import java.util.Scanner;

/**
 * Created by themba on 10/8/2015.
 */
public class ForceStringSearch {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ForceStringSearch fss = new ForceStringSearch();
        String text = "I'm learning java fundamental";
        System.out.print("Enter word you would like to search: ");
        String pattern = sc.next();

        fss.setString(text, pattern);
        int position = fss.search();

        if(position != -1)
            System.out.println("The text: "+ pattern + " is found at position no:" + position);
        else
            System.out.print("The text: " + pattern + " - was not found at all");
    }
        char[] text, pattern;
            int intText, intPattern;
    public void setString(String textString,String patternString){
        text = textString.toCharArray();
        pattern = patternString.toCharArray();
        intText = textString.length();
        intPattern = patternString.length();
    }
    public int search() {
        for (int i = 0; i < intText - intPattern; i++) {
            int j = 0;
            while (j < intPattern && text[i+j] == pattern[j]) {
                j++;
            }
            if (j == intPattern) return i;
        }
        return -1;
    }
}