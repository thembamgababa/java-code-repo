/**
 * Created by themba on 9/21/2015.
 */

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a name: ");
        String str = s.nextLine();
        System.out.print("Enter an number: ");
        int i = Integer.parseInt(s.next());
    }
}