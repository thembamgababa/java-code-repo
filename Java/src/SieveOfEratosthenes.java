import java.util.ArrayList;

/**
 * Created by themba on 10/8/2015.
 */
public class SieveOfEratosthenes {
    public static long arg= 100L ;


    public static void main(String args[])
    {
        Function(arg);
    }
    public static void Function(long arg)
    {
        ArrayList<Long> myPrimeList = new ArrayList<Long>();
        ArrayList<Long> myTempPrimeList = new ArrayList<Long>();
        ArrayList<Boolean> isPrimeNumber = new ArrayList<Boolean>();
        int index = 0;

        long maximum = arg;
        long minimum = 2;

        do
        {
            myTempPrimeList.add(minimum);
            isPrimeNumber.add(true);
            minimum++;
        }while( minimum != (arg+1));

        for(long i : myTempPrimeList)
        {
            if(isPrimeNumber.get(index))
            {
                myPrimeList.add(i);
                for(long j = i ; j*i <= maximum; j++)
                {
                    isPrimeNumber.set(myTempPrimeList.indexOf(j*i),false);
                }
            }
            index++;
        }

        System.out.println(myPrimeList);
    }
}



