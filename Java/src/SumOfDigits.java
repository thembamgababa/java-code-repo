/**
 * Created by themba on 9/22/2015.
 */
import java.util.Scanner;
  public class SumOfDigits {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int n;
            System.out.print("Enter a digits: ");
            n = in.nextInt();
            if (n <= 0)
                System.out.println("Digits you've entered is not supported.");
            else {
                int sum = 0;
                while (n != 0) {
                    sum += n % 10;
                    n /= 10;
                }
                System.out.println("Sum of digits: " + sum);
            }
        }
  }

