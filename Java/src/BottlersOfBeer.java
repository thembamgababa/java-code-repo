/**
 * Created by themba on 9/22/2015.
 */
public class BottlersOfBeer {
    public static void main(String[]args){
        System.out.println("99 bottles of beer on the wall");
        System.out.println("99 bottles of beer");
        System.out.println("Take one down, pass it around");
        System.out.println("98 bottles of beer on the wall");
        System.out.println("98 bottles of beer");
        System.out.println("Take one down, pass it around");
        System.out.println("97 bottles of beer on the wall");
        System.out.println("---");
        System.out.println("Take one down, pass it around");
        System.out.println("0 bottles of beer on the wall");

    }
}
