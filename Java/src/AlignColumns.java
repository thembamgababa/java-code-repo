/**
 * Created by themba on 9/22/2015.
 */
import java.awt.datatransfer.SystemFlavorMap;
public class AlignColumns {
    public static void main(String[]args){
        String[] information = new
                String[]{"Given$a$text$file$of$many$lines, $where$fields$within$a$line$",
                "are$delineated$by$a$single$\'dollar\'$character, $write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each$",
                "column$are$separated$by$at$least$one$space",
                "Further, $allow$for$each$word$in$a$column$to$be$either$left$",
                " justified, $right$justified, $or$center$justified$within$its$column"};
        String[] splitedInfo = null;
        System.out.println("left justified column");
        String[] arr$ = information;
        int len$ = information.length;

        int i$;
        String info;
        String[] arr$1;
        int len$1;
        int i$1;
        String k;
        for (i$ = 0; i$ < len$; ++i$) {
            info = arr$[i$];
            splitedInfo = info.split("\\$");
            arr$1 = splitedInfo;
            len$1 = splitedInfo.length;

            for (i$1 = 0; i$1 < len$1; ++i$1) {
                k = arr$1[i$1];
                System.out.printf("%-15s", new Object[]{k});
            }

            System.out.println();
        }
        System.out.println("\nRight justified column");
        arr$ = information;
        len$ = information.length;

        for (i$ = 0; i$ < len$; ++i$) {
            info = arr$[i$];
            splitedInfo = info.split("\\$");
            arr$1 = splitedInfo;
            len$1 = splitedInfo.length;

            for (i$1 = 0; i$1 < len$1; ++i$1) {
                k = arr$1[i$1];
                System.out.printf("%15s", new Object[]{k});
            }

            System.out.println();
        }

        System.out.println("\nCenter justified column");
        arr$ = information;
        len$ = information.length;

        for (i$ = 0; i$ < len$; ++i$) {
            info = arr$[i$];
            arr$1 = splitedInfo;
            splitedInfo = info.split("\\$");
            len$1 = splitedInfo.length;

            for (i$1 = 0; i$1 < len$1; ++i$1) {
                k = arr$1[i$1];

                System.out.printf("%s", new Object[]{k});
            }

            System.out.println();
        }
    }
}