import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by themba on 10/9/2015.
 */
public class TakingNotes {
    public static void main(String[] args){
        try {
            Date date = new Date() ;
            SimpleDateFormat dateFormat = new SimpleDateFormat("DD-MM-YYYY-HH-MM-SS") ;
            String content = dateFormat.format(date);

            File file = new File("C:\\Users\\themba\\Documents\\java-code-repo\\Java\\src\\TakingNotes.txt");
            if (!file.exists()) {
                file.createNewFile();
                System.out.println("File created successfully");
            }
            else if (file.exists() && file.length() == 0) {
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.newLine();
                bw.close();

                System.out.println("File successfully");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

