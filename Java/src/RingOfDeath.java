import java.util.ArrayList;

/**
 * Created by themba on 10/8/2015.
 */
public class RingOfDeath {
    public static void main(String[] args){
        System.out.println("Josephus value: " + josephus(41, 3));

    }

    public static int josephus(int n, int m){

        int kill = 0;
        ArrayList<Integer> prisoners = new ArrayList<>(n);
        for(int i = 0;i < n;i++){
            prisoners.add(i);
        }
        System.out.println("Prisoners executed in order:");
        while(prisoners.size() > 1){
            kill = (kill + m - 1) % prisoners.size();
            System.out.print(prisoners.get(kill) + " ");
            prisoners.remove(kill);
        }


        System.out.println();

        return prisoners.get(0);
    }
}

