import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by themba on 10/1/2015.
 */
public class Anagrams {
    public static void main(String[] args)
    {
        try
        {
            URL myUrl = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
            InputStreamReader isr = new InputStreamReader(myUrl.openStream());
            BufferedReader br = new BufferedReader(isr);

            String line = null;
            Scanner scan = new Scanner(System.in);
            String word = scan.next();
            int count = 0;
            while((line = br.readLine()) != null)
            {
                if(word.length() == line.length()) {
                    String sortedWord = sortCharacter(word);
                    String sortedLine = sortCharacter(line);

                    if (sortedWord.equalsIgnoreCase(sortedLine)) {
                        System.out.print(line);
                    }
                }
                count++;


            }


        }
        catch (Exception e)
        {

        }
    }
    public static String sortCharacter(String value)
    {

        char[] charArray = value.toCharArray();
        Arrays.sort(charArray);
        String valueToReturn = String.valueOf(charArray);
        return valueToReturn;
    }
}
